insert into writer (name) values ('Johnny Cash');
insert into writer (name) values ('John Snow');

insert into post (content, post_status, writer_id) values ('news', 'ACTIVE', 1);
insert into post (content, post_status, writer_id) values ('work', 'DELETED', 1);
insert into post (content, post_status, writer_id) values ('money', 'ACTIVE', 2);

insert into tag(name) values ('breaking');
insert into tag(name) values ('boring');
insert into tag(name) values ('fast');
insert into tag(name) values ('hot');
insert into tag(name) values ('stock');

insert into posts_tags(post_id, tag_id) values (1, 1);
insert into posts_tags(post_id, tag_id) values (2, 2);
insert into posts_tags(post_id, tag_id) values (3, 3);
insert into posts_tags(post_id, tag_id) values (1, 4);
insert into posts_tags(post_id, tag_id) values (3, 5);