drop table if exists tag;
drop table if exists post;
drop table if exists writer;

create table writer (
                        id bigint auto_increment primary key,
                        name varchar(30)
);

create table post (
                      id bigint auto_increment primary key,
                      content varchar(100),
                      post_status varchar(30),
                      writer_id bigint,
                      foreign key (writer_id) references writer (id) on delete cascade
);

create table tag (
                     id bigint auto_increment primary key,
                     name varchar(30)
);

create table posts_tags (
                            post_id bigint not null,
                            foreign key (post_id) references post(id) on delete cascade,
                            tag_id bigint not null,
                            foreign key (tag_id) references tag(id) on delete cascade,
                            unique (post_id, tag_id)
);

insert into writer (name) values ('Johnny Cash');
insert into writer (name) values ('John Snow');

insert into post (content, post_status, writer_id) values ('news', 'ACTIVE', 1);
insert into post (content, post_status, writer_id) values ('work', 'DELETED', 1);
insert into post (content, post_status, writer_id) values ('money', 'ACTIVE', 2);

insert into tag(name) values ('breaking');
insert into tag(name) values ('boring');
insert into tag(name) values ('fast');
insert into tag(name) values ('hot');
insert into tag(name) values ('stock');

insert into posts_tags(post_id, tag_id) values (1, 1);
insert into posts_tags(post_id, tag_id) values (2, 2);
insert into posts_tags(post_id, tag_id) values (3, 3);
insert into posts_tags(post_id, tag_id) values (1, 4);
insert into posts_tags(post_id, tag_id) values (3, 5);

