package controllers;

import models.Tag;
import services.TagsService;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class TagsController {

    private final TagsService tagsService;

    public TagsController() {
        this.tagsService = new TagsService();
    }

    public List<Tag> findAll() {
        return tagsService.findAll();
    }

    /**
     * @param data - Должно приходить в формате "1", где 1 - это id Tag-a
     */
    public Tag findById(String data) {
        if (data.matches("\\d+")) {
            Long tagId = Long.valueOf(data);
            Optional<Tag> optionalTag = tagsService.getById(tagId);
            if (optionalTag.isPresent()) {
                return optionalTag.get();
            } else {
                throw new IllegalArgumentException("Tag with this id doesn't exist");
            }
        } else {
            throw new IllegalArgumentException("Wrong number format");
        }
    }

    public Tag save(String tagName) {
        Tag tag = Tag.builder()
                .name(tagName)
                .build();
        return tagsService.save(tag);
    }

    /**
     * @param data - Должно приходить в формате "1, something", где 1 - это id Tag-a; something - это newName
     */
    public Tag update(String tagId, String data) {
        if (tagId.matches("\\d+")) {
            Tag tag = Tag.builder()
                    .id(Long.valueOf(tagId))
                    .name(data)
                    .build();
            return tagsService.update(tag);
        } else {
            throw new IllegalStateException("Wrong number format");
        }
    }

    /**
     * @param data - Должно приходить в формате "1", где 1 - это id Tag-a
     */
    public void delete(String data) {
        if (data.matches("\\d+")) {
            Long tagId = Long.valueOf(data);
            tagsService.delete(tagId);
            System.out.println("Deleted");
        } else {
            throw new IllegalArgumentException("Wrong number format");
        }
    }

    /**
     * @param data - Должно приходить в формате "1, 2, 3", где цифры - это айди тегов
     */
    public void deleteTagsByIds(String postId, String data) {
        String[] splitedData = data.split(", ");
        if (postId.matches("\\d+")) {
            Long id = Long.valueOf(postId);
            List<Long> numbersOfTagId = new ArrayList<>();
            for (String s : splitedData) {
                if (s.matches("\\d+")) {
                    numbersOfTagId.add(Long.valueOf(s));
                } else {
                    throw new IllegalArgumentException("Wrong number format");
                }
            }
            tagsService.deleteTagsFromPostByPostId(id, numbersOfTagId);
            //так как при добавлении в таблицу у меня нет уникальности по имени тега, то здесь нужно дополнительно удалить и
            //сам тег из таблицы
            tagsService.deleteTagsByListOfTagsId(numbersOfTagId);
        } else {
            throw new IllegalArgumentException("Wrong number format");
        }
    }


    /**
     * @param postId - это id поста
     * @param data   - Должно приходить в формате "tag, tag, tag", где tag - это новый тег поста
     */
    public void addTags(String postId, String data) {
        String[] splitedData = data.split(", ");
        if (postId.matches("\\d+")) {
            Long id = Long.valueOf(postId);
            List<Long> numbersOfTagId = new ArrayList<>();
            for (int i = 0; i < splitedData.length; i++) {
                Tag tag = Tag.builder()
                        .name(splitedData[i])
                        .build();
                tagsService.save(tag);
                numbersOfTagId.add(tag.getId());
            }
            tagsService.addTagsToPost(id, numbersOfTagId);
        }
    }
}
