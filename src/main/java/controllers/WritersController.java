package controllers;

import models.Writer;
import services.WritersService;

import java.util.List;
import java.util.Optional;

public class WritersController {

    private final WritersService writersService;


    public WritersController() {
        this.writersService = new WritersService();
    }


    public List<Writer> findAll() {
        return writersService.findAll();
    }


    /**
     * @param data - от пользователя в консоли приходит только id Writer-a
     *             проверяет существует ли Writer с таким id
     */
    public Writer getById(String data) {
        if (data.matches("\\d+")) {
            Long writerId = Long.valueOf(data);
            Optional<Writer> writer = writersService.getById(writerId);
            if (writer.isPresent()) {
                return writer.get();
            } else {
                throw new IllegalArgumentException("Can't find writer with this id");
            }
        } else {
            throw new IllegalArgumentException("Wrong number format");
        }
    }

    public Writer save(String data) {
        Writer writer = Writer.builder()
                .name(data)
                .build();
        return writersService.save(writer);
    }

    /**
     * @param writerId - это id писателя
     * @param newName - это новое имя писателя
     */
    public Writer update(String writerId, String newName) {
        Writer writer = getById(writerId);
        writer.setName(newName);
        return writersService.update(writer);
    }

    /**
     *
     * @param data - id Writer-a
     * Удаление Writer-a из базы
     */
    public void delete(String data) {
        if (data.matches("\\d+")){
            Long writerId = Long.valueOf(data);
            writersService.deleteById(writerId);
        } else {
            throw new IllegalArgumentException("Wrong number format");
        }
    }
}
