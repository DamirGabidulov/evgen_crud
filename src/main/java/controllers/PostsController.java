package controllers;

import models.Post;
import models.PostStatus;
import services.PostsService;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class PostsController {
    private final PostsService postsService;

    public PostsController() {
        this.postsService = new PostsService();
    }

    public List<Post> findAll() {
        return postsService.findAll();
    }

    /**
     * @param data - Должно приходить в формате "1", где 1 - это id Post-a
     */
    public Post getById(String data) {
        if (data.matches("\\d+")) {
            Long id = Long.valueOf(data);
            Optional<Post> optionalPost = postsService.getById(id);
            if (optionalPost.isPresent()) {
                return optionalPost.get();
            } else {
                throw new IllegalArgumentException("Post with this id doesn't exist");
            }
        } else {
            throw new IllegalArgumentException("Wrong number format");
        }
    }

    public Post save(String data) {
        Post post = Post.builder()
                .content(data)
                .status(PostStatus.ACTIVE)
                .build();
        return postsService.save(post);
    }

    /**
     * @param id             - это id поста
     * @param updatedContent - это новый контент поста
     */
    public Post updateContent(String id, String updatedContent) {
        Post post = getById(id);
        post.setContent(updatedContent);
        return postsService.update(post);
    }

    /**
     * @param data - id поста который нужно удалить
     *             пост не удаляет физически из базы, только ставится статус DELETED. Далее посты с таким статусом в поиске не высвечиваются
     */
    public void delete(String data) {
        if (data.matches("\\d+")) {
            Long postId = Long.valueOf(data);
            postsService.delete(postId);
        } else {
            throw new IllegalArgumentException("Wrong number format");
        }
    }

    public Post addNewPostToWriter(String writerId, String data) {
        if (writerId.matches("\\d+")) {
            Post post = Post.builder()
                    .content(data)
                    .status(PostStatus.ACTIVE)
                    .build();
            return postsService.addNewPostToWriter(Long.valueOf(writerId), post);
        } else {
            throw new IllegalArgumentException("Wrong number format");
        }
    }

    public void deletePostsFromWriterByIds(String data) {
        String[] splitedData = data.split(", ");
            List<Long> numbersOfPostId = new ArrayList<>();
            for (String s : splitedData) {
                if (s.matches("\\d+")) {
                    numbersOfPostId.add(Long.valueOf(s));
                } else {
                    throw new IllegalArgumentException("Wrong number format");
                }
            }
            postsService.deletePostsFromWriterByIds(numbersOfPostId);
    }
}
