package repositories.jdbc;

import models.Post;
import models.PostStatus;
import models.Tag;
import repositories.PostRepository;
import utils.JdbcUtil;

import java.sql.*;
import java.util.*;
import java.util.function.Function;

import static utils.JdbcUtil.getConnection;
import static utils.Mapper.postMap;
import static utils.Mapper.postWithTagsMapper;

public class PostRepositoryImpl implements PostRepository {

    //language=SQL
    private static final String SQL_SELECT_ALL = "select post_id, content, post_status, writer_id, pt.tag_id as tag_id, name as tag_name " +
            "from post left join posts_tags as pt on post.id = pt.post_id " +
            "left join tag t on pt.tag_id = t.id " +
            "where post_status = ?";

    //language=SQL
    private static final String SQL_GET_BY_ID = "select post_id, content, post_status, writer_id, pt.tag_id as tag_id, name as tag_name " +
            "from post " +
            "left join posts_tags as pt on post.id = pt.post_id " +
            "left join tag t on pt.tag_id = t.id " +
            "where post.id = ? " +
            "and post_status = ?";

    //language=SQL
    private static final String SQL_SAVE = "insert into post (content, post_status) values (?, ?)";

    //language=SQL
    private static final String SQL_ADD_NEW_POST_TO_WRITER = "insert into post (content, post_status, writer_id) values (?, ?, ?)";

    //language=SQL
    private static final String SQL_UPDATE = "update post set content = ? where id = ? and post_status = ?";

    //language=SQL
    private static final String SQL_DELETE = "update post set post_status = ? where id = ?";


    @Override
    public List<Post> findAll() {
        postMap = new HashMap<>();

        try (PreparedStatement statement = JdbcUtil.prepareStatement(SQL_SELECT_ALL)) {
            statement.setString(1, PostStatus.ACTIVE.toString());

            ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()) {
                postWithTagsMapper.apply(resultSet);
            }
            return new ArrayList<>(postMap.values());

        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public Optional<Post> getById(Long id) {
        postMap = new HashMap<>();

        try (PreparedStatement statement = JdbcUtil.prepareStatement(SQL_GET_BY_ID)) {
            statement.setLong(1, id);
            statement.setString(2, PostStatus.ACTIVE.toString());

            ResultSet resultSet = statement.executeQuery();
            if (resultSet.isBeforeFirst()) {
                while (resultSet.next()) {
                    postWithTagsMapper.apply(resultSet);
                }
                return Optional.of(postMap.get(id));
            } else {
                return Optional.empty();
            }

        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public Post save(Post post) {
        try (PreparedStatement preparedStatement = JdbcUtil.prepareStatement(SQL_SAVE, Statement.RETURN_GENERATED_KEYS)) {
            preparedStatement.setString(1, post.getContent());
            preparedStatement.setString(2, post.getStatus().toString());

            int affectedRows = preparedStatement.executeUpdate();
            if (affectedRows != 1) {
                throw new SQLException("Can't insert data");
            }

            try (ResultSet generatedKeys = preparedStatement.getGeneratedKeys()) {
                if (generatedKeys.next()) {
                    post.setId(generatedKeys.getLong(1));
                    return post;
                } else {
                    throw new SQLException("Can't get id");
                }
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public Post update(Post post) {
        try (PreparedStatement preparedStatement = JdbcUtil.prepareStatement(SQL_UPDATE)) {
            preparedStatement.setString(1, post.getContent());
            preparedStatement.setLong(2, post.getId());
            preparedStatement.setString(3, post.getStatus().toString());

            int affectedRows = preparedStatement.executeUpdate();
            if (affectedRows != 1) {
                throw new SQLException("Can't update post");
            }
            return post;
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public void deleteById(Long postId) {
        try (PreparedStatement preparedStatement = JdbcUtil.prepareStatement(SQL_DELETE)) {
            preparedStatement.setString(1, PostStatus.DELETED.toString());
            preparedStatement.setLong(2, postId);

            int affectedRows = preparedStatement.executeUpdate();
            if (affectedRows != 1) {
                throw new SQLException("Can't delete post with this id");
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public Post addNewPostToWriter(Long writerId, Post post) {
        try (PreparedStatement preparedStatement = JdbcUtil.prepareStatement(SQL_ADD_NEW_POST_TO_WRITER, Statement.RETURN_GENERATED_KEYS)) {
            preparedStatement.setString(1, post.getContent());
            preparedStatement.setString(2, post.getStatus().toString());
            preparedStatement.setLong(3, writerId);

            int affectedRows = preparedStatement.executeUpdate();
            if (affectedRows != 1) {
                throw new SQLException("Can't insert data");
            }

            try (ResultSet generatedKeys = preparedStatement.getGeneratedKeys()) {
                if (generatedKeys.next()) {
                    post.setId(generatedKeys.getLong(1));
                    return post;
                } else {
                    throw new SQLException("Can't get id");
                }
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public void deletePostsFromWriterByIds(List<Long> numbersOfPostId) {
        try (PreparedStatement preparedStatement = JdbcUtil.prepareStatement(SQL_DELETE)) {
            getConnection().setAutoCommit(false);

            for (Long postId : numbersOfPostId) {
                preparedStatement.setString(1, PostStatus.DELETED.toString());
                preparedStatement.setLong(2, postId);
                preparedStatement.addBatch();
            }
            preparedStatement.executeBatch();
            getConnection().commit();
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }
}
