package repositories.jdbc;

import models.PostStatus;
import models.Writer;
import repositories.WritersRepository;
import utils.JdbcUtil;

import java.sql.*;
import java.util.*;

import static utils.JdbcUtil.getConnection;
import static utils.Mapper.*;

public class WritersRepositoryImpl implements WritersRepository {

    //language=SQL
    private static final String SQL_FIND_ALL = "select writer_id, writer.name as writer_name, post_id, content, post_status, tag_id, t.name as tag_name from writer " +
            "left join post p on writer.id = p.writer_id " +
            "left join posts_tags as pt on p.id = pt.post_id " +
            "left join tag t on pt.tag_id = t.id where post_status = ?";

    //language=SQL
    private static final String SQL_GET_BY_ID = "select writer_id, writer.name as writer_name, p.id as post_id, content, post_status, tag_id, t.name as tag_name from writer " +
            "left join post p on writer.id = p.writer_id " +
            "left join posts_tags as pt on p.id = pt.post_id " +
            "left join tag t on pt.tag_id = t.id where writer_id = ? and post_status = ?";

    //language=SQL
    private static final String SQL_SAVE = "insert into writer (name) values (?)";

    //language=SQL
    private static final String SQL_UPDATE = "update writer set name = ? where id = ?";

    //language=SQL
    private static final String SQL_DELETE = "delete from writer where id = ?";

    @Override
    public List<Writer> findAll() {
        postMap = new HashMap<>();
        writerMap = new HashMap<>();
        try(PreparedStatement preparedStatement = JdbcUtil.prepareStatement(SQL_FIND_ALL)){
            preparedStatement.setString(1, PostStatus.ACTIVE.toString());
            try(ResultSet resultSet = preparedStatement.executeQuery()){
                while (resultSet.next()){
                    writerWithPostsMapper.apply(resultSet);
                }
            }
            return new ArrayList<>(writerMap.values());
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public Optional<Writer> getById(Long id) {
        postMap = new HashMap<>();
        writerMap = new HashMap<>();
        try(PreparedStatement preparedStatement = JdbcUtil.prepareStatement(SQL_GET_BY_ID)){
            preparedStatement.setLong(1, id);
            preparedStatement.setString(2, PostStatus.ACTIVE.toString());
            try (ResultSet resultSet = preparedStatement.executeQuery()){
                //проверка что resultSet не пустой
                if (resultSet.isBeforeFirst()){
                    while (resultSet.next()){
                        writerWithPostsMapper.apply(resultSet);
                    }
                    return Optional.of(writerMap.get(id));
                } else {
                    return Optional.empty();
                }
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public Writer save(Writer writer) {
        try (PreparedStatement preparedStatement = JdbcUtil.prepareStatement(SQL_SAVE, Statement.RETURN_GENERATED_KEYS)) {
            preparedStatement.setString(1, writer.getName());

            int affectedRows = preparedStatement.executeUpdate();
            if (affectedRows != 1) {
                throw new SQLException("Can't insert data");
            }
            try (ResultSet resultSet = preparedStatement.getGeneratedKeys()) {
                if (resultSet.next()) {
                    writer.setId(resultSet.getLong(1));
                    return writer;
                } else {
                    throw new SQLException("Can't get id");
                }
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public Writer update(Writer writer) {
        try (PreparedStatement preparedStatement = JdbcUtil.prepareStatement(SQL_UPDATE)) {
            preparedStatement.setString(1, writer.getName());
            preparedStatement.setLong(2, writer.getId());

            int affectedRows = preparedStatement.executeUpdate();
            if (affectedRows != 1){
                throw new SQLException("Can't update data");
            }

            Optional<Writer> optionalWriter = getById(writer.getId());
            if (optionalWriter.isPresent()){
                return optionalWriter.get();
            } else {
                throw new IllegalArgumentException("Writer with this id doesn't exist");
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public void deleteById(Long id) {
        try (PreparedStatement preparedStatement = JdbcUtil.prepareStatement(SQL_DELETE)){
            preparedStatement.setLong(1, id);

            int affectedRows = preparedStatement.executeUpdate();
            if (affectedRows != 1){
                throw new SQLException("Can't delete writer");
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }
}
