package repositories.jdbc;

import models.Post;
import models.Tag;
import repositories.TagRepository;
import utils.JdbcUtil;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.function.Function;

import static utils.JdbcUtil.createStatement;
import static utils.JdbcUtil.getConnection;
import static utils.Mapper.tagMapper;

public class TagRepositoryImpl implements TagRepository {

    //language=SQL
    private static final String SQL_FIND_ALL = "select id as tag_id, name as tag_name from tag";

    //language=SQL
    private static final String SQL_GET_BY_ID = "select id as tag_id, name as tag_name from tag where tag.id = ?";

    //language=SQL
    private static final String SQL_SAVE = "insert into tag (name) values (?)";

    //language=SQL
    private static final String SQL_UPDATE = "update tag set name = ? where id = ?";

    //language=SQL
    private static final String SQL_DELETE = "delete from tag where id = ?";

    //language=SQL
    private static final String SQL_DELETE_FROM_POSTS_TAGS = "delete from posts_tags where tag_id = ?; ";

    //language=SQL
    private static final String SQL_ADD_TAGS_TO_POST = "insert into posts_tags (post_id, tag_id) values (?, ?)";

    //language=SQL
    private static final String SQL_DELETE_TAGS_FROM_POST = "delete from posts_tags where post_id = ? and tag_id = ?";

    @Override
    public List<Tag> findAll() {
        List<Tag> tags = new ArrayList<>();

        try (Statement statement = JdbcUtil.createStatement()) {
            try (ResultSet resultSet = statement.executeQuery(SQL_FIND_ALL)) {
                while (resultSet.next()) {
                    tags.add(tagMapper.apply(resultSet));
                }
            }
            return tags;
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public Optional<Tag> getById(Long id) {
        try (PreparedStatement preparedStatement = JdbcUtil.prepareStatement(SQL_GET_BY_ID)) {
            preparedStatement.setLong(1, id);
            try (ResultSet resultSet = preparedStatement.executeQuery()) {
                if (resultSet.next()) {
                    return Optional.of(tagMapper.apply(resultSet));
                } else {
                    return Optional.empty();
                }
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public Tag save(Tag tag) {
        try (PreparedStatement preparedStatement = JdbcUtil.prepareStatement(SQL_SAVE, Statement.RETURN_GENERATED_KEYS)) {
            preparedStatement.setString(1, tag.getName());

            int affectedRows = preparedStatement.executeUpdate();
            if (affectedRows != 1) {
                throw new SQLException("Can't insert data");
            }

            try (ResultSet generatedKeys = preparedStatement.getGeneratedKeys()) {
                if (generatedKeys.next()) {
                    tag.setId(generatedKeys.getLong(1));
                } else {
                    throw new SQLException("Can't get id");
                }
            }

            return tag;

        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public Tag update(Tag tag) {
        try (PreparedStatement preparedStatement = JdbcUtil.prepareStatement(SQL_UPDATE)) {

            preparedStatement.setString(1, tag.getName());
            preparedStatement.setLong(2, tag.getId());

            int affectedRows = preparedStatement.executeUpdate();
            if (affectedRows != 1) {
                throw new SQLException("Can't update data");
            }

            return tag;
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public void deleteById(Long id) {
        try (PreparedStatement preparedStatement = JdbcUtil.prepareStatement(SQL_DELETE)){
            preparedStatement.setLong(1, id);

            int affectedRows = preparedStatement.executeUpdate();
            if (affectedRows != 1){
                throw new SQLException("Can't delete tag");
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public void addTagsToPost(Long postId, List<Long> numbersOfTagId) {
        try (PreparedStatement preparedStatement = JdbcUtil.prepareStatement(SQL_ADD_TAGS_TO_POST)) {
            getConnection().setAutoCommit(false);

            for (Long tag : numbersOfTagId) {
                preparedStatement.setLong(1, postId);
                preparedStatement.setLong(2, tag);
                preparedStatement.addBatch();
            }
            preparedStatement.executeBatch();
            getConnection().commit();
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public void deleteTagsFromPostByPostId(Long postId, List<Long> numbersOfTagId) {
        try (PreparedStatement preparedStatement = JdbcUtil.prepareStatement(SQL_DELETE_TAGS_FROM_POST)) {
            getConnection().setAutoCommit(false);

            for (Long tag : numbersOfTagId) {
                preparedStatement.setLong(1, postId);
                preparedStatement.setLong(2, tag);
                preparedStatement.addBatch();
            }
            preparedStatement.executeBatch();
            getConnection().commit();
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public void deleteTagsByListOfTagsId(List<Long> numbersOfTagId) {
        try (PreparedStatement preparedStatement = JdbcUtil.prepareStatement(SQL_DELETE)) {
            getConnection().setAutoCommit(false);

            for (Long tag : numbersOfTagId) {
                preparedStatement.setLong(1, tag);
                preparedStatement.addBatch();
            }
            preparedStatement.executeBatch();
            getConnection().commit();
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }
}
