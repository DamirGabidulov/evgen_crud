package repositories;

import models.Tag;

import java.util.List;
import java.util.Optional;

public interface TagRepository extends GenericRepository<Tag, Long> {

    @Override
    List<Tag> findAll();

    @Override
    Optional<Tag> getById(Long id);

    @Override
    Tag save(Tag tag);

    @Override
    Tag update(Tag tag);

    @Override
    void deleteById(Long id);

    void addTagsToPost(Long postId, List<Long> numbersOfTagId);

    void deleteTagsFromPostByPostId(Long postId, List<Long> numbersOfTagId);

    void deleteTagsByListOfTagsId(List<Long> numbersOfTagId);


}
