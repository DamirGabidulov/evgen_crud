package repositories;

import models.Post;

import java.util.List;
import java.util.Optional;

public interface GenericRepository<T, ID> {

    List<T> findAll();

    Optional<T> getById(ID id);

    T save(T t);

    T update(T t);

    void deleteById(ID id);
}
