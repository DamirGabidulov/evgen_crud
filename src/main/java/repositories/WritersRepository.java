package repositories;

import models.Writer;

import java.util.List;
import java.util.Optional;

public interface WritersRepository extends GenericRepository<Writer, Long>{

    @Override
    List<Writer> findAll();

    @Override
    Optional<Writer> getById(Long id);

    @Override
    Writer save(Writer writer);

    @Override
    Writer update(Writer writer);

    @Override
    void deleteById(Long id);
}
