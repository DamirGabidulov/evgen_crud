package repositories;

import models.Post;

import java.util.List;
import java.util.Optional;

public interface PostRepository extends GenericRepository<Post, Long>{

    @Override
    List<Post> findAll();

    @Override
    Optional<Post> getById(Long id);

    @Override
    Post save(Post post);

    @Override
    Post update(Post post);

    @Override
    void deleteById(Long id);

    Post addNewPostToWriter(Long writerId, Post post);

    void deletePostsFromWriterByIds(List<Long> numbersOfPostId);
}
