package utils;

import models.Post;
import models.PostStatus;
import models.Tag;
import models.Writer;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Map;
import java.util.function.Function;

public class Mapper {

    public static Map<Long, Writer> writerMap;
    public static Map<Long, Post> postMap;
    public static final Function<ResultSet, Writer> writerMapper = resultSet -> {
        try {
            Long id = resultSet.getLong("writer_id");
            String name = resultSet.getString("writer_name");
            return Writer.builder()
                    .id(id)
                    .name(name)
                    .build();
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    };

    public static final Function<ResultSet, Post> postMapper = resultSet -> {
        try {
            Long id = resultSet.getLong("post_id");
            String content = resultSet.getString("content");
            String status = resultSet.getString("post_status");
            return Post.builder()
                    .id(id)
                    .content(content)
                    .status(PostStatus.valueOf(status))
                    .build();
        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        }
    };

    public static final Function<ResultSet, Tag> tagMapper = resultSet -> {
        try {
            Long tagId = resultSet.getLong("tag_id");
            String name = resultSet.getString("tag_name");
            return Tag.builder()
                    .id(tagId)
                    .name(name)
                    .build();
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    };

    public static final Function<ResultSet, Post> postWithTagsMapper = resultSet -> {
        try {
            Long postId = resultSet.getLong("post_id");
            if (!postMap.containsKey(postId)) {
                Post post = postMapper.apply(resultSet);
                post.setTags(new ArrayList<>());
                postMap.put(postId, post);
            }
            Tag tag = tagMapper.apply(resultSet);
            Post post = postMap.get(postId);
            post.getTags().add(tag);
            return post;
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    };

    public static final Function<ResultSet, Writer> writerWithPostsMapper = resultSet -> {
        try {
            Long writerId = resultSet.getLong("writer_id");
            if (!writerMap.containsKey(writerId)) {
                Writer writer = writerMapper.apply(resultSet);
                writer.setPosts(new ArrayList<>());
                writerMap.put(writerId, writer);
            }
            if (!postMap.containsKey(resultSet.getLong("post_id"))) {
                Post post = postWithTagsMapper.apply(resultSet);
                Writer writer = writerMap.get(writerId);
                writer.getPosts().add(post);
                return writer;
            } else {
                postWithTagsMapper.apply(resultSet);
                Writer writer = writerMap.get(writerId);
                return writer;
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    };
}
