package app;

import view.NewGeneralView;

public class Main {
    public static void main(String[] args) {
        NewGeneralView generalView = new NewGeneralView();
        generalView.start();
    }
}
