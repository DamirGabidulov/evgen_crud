package view;

import controllers.TagsController;

import java.util.Scanner;

public class NewTagView {
    private final TagsController tagsController;
    private Scanner scanner = new Scanner(System.in);
    private String data = null;

    public NewTagView() {
        tagsController = new TagsController();
    }

    public void doAction(String command) {
        switch (command) {
            case "1":
                System.out.println("Введите данные");
                data = scanner.nextLine();
                save(data);
                break;
            case "2":
                findAll();
                break;
            case "3":
                System.out.println("Введите данные");
                data = scanner.nextLine();
                getById(data);
                break;
            case "4":
                System.out.println("Введите id тега который хотите обновить");
                String tagId = scanner.nextLine();
                getById(tagId);
                System.out.println("Введите новое имя");
                data = scanner.nextLine();
                update(tagId, data);
                break;
            case "5":
                System.out.println("Введите id тега который хотите удалить");
                data = scanner.nextLine();
                delete(data);
                break;
        }
    }

    private void findAll() {
        System.out.println(tagsController.findAll());
    }

    private void getById(String data) {
        System.out.println(tagsController.findById(data));
    }

    private void save(String data) {
        System.out.println(tagsController.save(data));
    }

    private void update(String tagId, String data) {
        System.out.println(tagsController.update(tagId, data));
    }

    private void delete(String data) {
        tagsController.delete(data);
    }
}
