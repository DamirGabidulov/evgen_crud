package view;

import controllers.PostsController;
import controllers.TagsController;
import controllers.WritersController;
import models.Post;

import java.util.Scanner;

public class NewWriterView {

    private final WritersController writersController;
    private final PostsController postsController;
    private final TagsController tagsController;
    private Scanner scanner = new Scanner(System.in);
    private String data = null;
    private String writerId = null;

    public NewWriterView() {
        writersController = new WritersController();
        postsController = new PostsController();
        tagsController = new TagsController();
    }

    public void doAction(String command) {
        switch (command) {
            case "1":
                System.out.println("Введите имя писателя");
                data = scanner.nextLine();
                save(data);
                System.out.println("Введите id писателя");
                writerId = scanner.nextLine();
                System.out.println("Введите новый пост");
                data = scanner.nextLine();
                Post post = addNewPostToWriter(writerId, data);
                System.out.println("Если хотите добавить теги к посту введите - 1, если нет - любой символ");
                data = scanner.nextLine();
                if (data.equals("1")){
                    System.out.println("Введите теги через запятую");
                    data = scanner.nextLine();
                    tagsController.addTags(post.getId().toString(), data);
                    getById(writerId);
                }
                break;
            case "2":
                findAll();
                break;
            case "3":
                System.out.println("Введите данные");
                data = scanner.nextLine();
                getById(data);
                break;
            case "4":
                System.out.println("Введите id писателя который хотите обновить");
                writerId = scanner.nextLine();
                getById(writerId);
                System.out.println("Если хотите обновить имя писателя введите - 1");
                System.out.println("Если хотите добавить пост писателю введите - 2");
                System.out.println("Если хотите удалить посты у писателя - 3");
                data = scanner.nextLine();
                if (data.equals("1")){
                    System.out.println("Введите новое имя");
                    data = scanner.nextLine();
                    update(writerId, data);
                } else if (data.equals("2")){
                    System.out.println("Введите новый пост");
                    data = scanner.nextLine();
                    Post newPost = addNewPostToWriter(writerId, data);
                    getById(writerId);
                    System.out.println("Если хотите добавить теги к посту введите - 1, если нет - любой символ");
                    data = scanner.nextLine();
                    if (data.equals("1")){
                        System.out.println("Введите теги через запятую");
                        data = scanner.nextLine();
                        tagsController.addTags(newPost.getId().toString(), data);
                        getById(writerId);
                    }
                } else if (data.equals("3")){
                    System.out.println("Введите айди постов через запятую");
                    data = scanner.nextLine();
                    deletePostsFromWriter(data);
                    getById(writerId);
                }
                break;
            case "5":
                System.out.println("Введите id писателя которого хотите удалить");
                data = scanner.nextLine();
                delete(data);
                break;
        }
    }

    private void findAll(){
        System.out.println(writersController.findAll());
    }

    private void getById(String data){
        System.out.println(writersController.getById(data));
    }

    private void save(String data){
        System.out.println(writersController.save(data));
    }

    private void update(String writerId, String data){
        System.out.println(writersController.update(writerId, data));
    }

    private void delete(String data){
        writersController.delete(data);
        System.out.println("Deleted");
    }

    private Post addNewPostToWriter(String writerId, String data) {
       return postsController.addNewPostToWriter(writerId, data);
    }
    private void deletePostsFromWriter(String data) {
        postsController.deletePostsFromWriterByIds(data);
    }

}
