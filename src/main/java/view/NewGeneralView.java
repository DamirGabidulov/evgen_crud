package view;

import java.util.Scanner;

public class NewGeneralView {

    private final NewTagView tagView;
    private final NewPostView postView;
    private final NewWriterView writerView;

    public NewGeneralView() {
        this.tagView = new NewTagView();
        this.postView = new NewPostView();
        this.writerView = new NewWriterView();
    }
    Scanner scanner = new Scanner(System.in);

    public void start(){
        while (true){
            System.out.println("\n -------------------------- \n");
            System.out.println("Выберите с чем хотите работать:");
            System.out.println("Писатель - введите writer");
            System.out.println("Пост - введите post");
            System.out.println("Тег - введите tag");
            String action = scanner.nextLine();
            doAction(action);
        }
    }

    String command = null;

    private void doAction(String action) {

        switch (action) {
            case "writer":
                System.out.println("Сохранить нового писателя - введите 1");
                System.out.println("Выбрать всех писателей - введите 2");
                System.out.println("Выбрать писателя по id - введите 3");
                System.out.println("Обновить данные у писателя - введите 4");
                System.out.println("Удалить писателя по id - введите 5");
                command = scanner.nextLine();
                writerView.doAction(command);
                break;
            case "post":
                System.out.println("Сохранить новый пост - введите 1");
                System.out.println("Выбрать все посты - введите 2");
                System.out.println("Выбрать пост по id - введите 3");
                System.out.println("Обновить данные у поста - введите 4");
                System.out.println("Удалить пост по id - введите 5");
                command = scanner.nextLine();
                postView.doAction(command);
                break;
            case "tag":
                System.out.println("Сохранить новый тег - введите 1");
                System.out.println("Выбрать все теги - введите 2");
                System.out.println("Выбрать тег по id - введите 3");
                System.out.println("Обновить данные у тега - введите 4");
                System.out.println("Удалить тег по id - введите 5");
                command = scanner.nextLine();
                tagView.doAction(command);
                break;
        }
    }
}
