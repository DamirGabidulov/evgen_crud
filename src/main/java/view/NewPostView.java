package view;

import controllers.PostsController;
import controllers.TagsController;

import java.util.Scanner;

public class NewPostView {
    private final PostsController postsController;
    private final TagsController tagsController;
    private Scanner scanner = new Scanner(System.in);
    private String data = null;

    public NewPostView() {
        postsController = new PostsController();
        tagsController = new TagsController();
    }

    public void doAction(String command) {
        switch (command) {
            case "1":
                System.out.println("Введите данные");
                data = scanner.nextLine();
                save(data);
                break;
            case "2":
                findAll();
                break;
            case "3":
                System.out.println("Введите данные");
                data = scanner.nextLine();
                getById(data);
                break;
            case "4":
                System.out.println("Введите id поста который хотите обновить");
                String postId = scanner.nextLine();
                getById(postId);
                System.out.println("Если хотите обновить контент введите - 1");
                System.out.println("Если хотите добавить теги к посту - 2");
                System.out.println("Если хотите удалить теги у поста - 3");
                data = scanner.nextLine();
                if (data.equals("1")){
                    System.out.println("Введите новый контент");
                    data = scanner.nextLine();
                    updateContent(postId, data);
                } else if (data.equals("2")){
                    System.out.println("Введите теги через запятую");
                    data = scanner.nextLine();
                    addTags(postId, data);
                    getById(postId);
                } else if (data.equals("3")){
                    System.out.println("Введите айди тегов через запятую");
                    data = scanner.nextLine();
                    deleteTags(postId, data);
                    getById(postId);
                }
                break;
            case "5":
                System.out.println("Введите id поста который хотите удалить");
                data = scanner.nextLine();
                delete(data);
                break;
        }
    }

    private void findAll() {
        System.out.println(postsController.findAll());
    }

    private void getById(String data){
        System.out.println(postsController.getById(data));
    }

    private void save(String data){
        System.out.println(postsController.save(data));
    }

    private void updateContent(String postId, String data){
        System.out.println(postsController.updateContent(postId, data));
    }

    private void delete(String data){
        postsController.delete(data);
        System.out.println("Deleted");
    }

    private void addTags(String postId, String data){
        tagsController.addTags(postId, data);
    }

    private void deleteTags(String postId, String data){
        tagsController.deleteTagsByIds(postId, data);
    }
}
