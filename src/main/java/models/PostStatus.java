package models;

public enum PostStatus {
    ACTIVE, DELETED
}
