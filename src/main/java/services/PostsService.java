package services;

import models.Post;
import repositories.PostRepository;
import repositories.jdbc.PostRepositoryImpl;
import repositories.TagRepository;
import repositories.jdbc.TagRepositoryImpl;

import java.util.List;
import java.util.Optional;

import static utils.JdbcUtil.getConnection;

public class PostsService {

    private PostRepository postRepository;

    public PostsService() {
        this.postRepository = new PostRepositoryImpl();
    }

    public void setPostRepository(PostRepository postRepository) {
        this.postRepository = postRepository;
    }

    public List<Post> findAll() {
        return postRepository.findAll();
    }

    public Optional<Post> getById(Long id){
        return postRepository.getById(id);
    }

    public Post save(Post post){
        return postRepository.save(post);
    }

    public Post update(Post post) {
        return postRepository.update(post);
    }


    public void delete(Long id) {
        postRepository.deleteById(id);
    }

    public Post addNewPostToWriter(Long writerId, Post post) {
        return postRepository.addNewPostToWriter(writerId, post);
    }

    public void deletePostsFromWriterByIds(List<Long> numbersOfPostId) {
        postRepository.deletePostsFromWriterByIds(numbersOfPostId);
    }
}
