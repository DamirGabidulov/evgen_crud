package services;

import models.Tag;
import repositories.TagRepository;
import repositories.jdbc.TagRepositoryImpl;

import java.util.List;
import java.util.Optional;

import static utils.JdbcUtil.getConnection;

public class TagsService {
    private TagRepository tagRepository;

    public TagsService() {
        this.tagRepository = new TagRepositoryImpl();
    }

    public void setTagRepository(TagRepository tagRepository){
        this.tagRepository = tagRepository;
    }

    public List<Tag> findAll() {
        return tagRepository.findAll();
    }

    public Optional<Tag> getById(Long tagId){
       return tagRepository.getById(tagId);
    }

    public Tag save(Tag tag) {
       return tagRepository.save(tag);
    }

    public Tag update(Tag tag){
        return tagRepository.update(tag);
    }

    public void delete(Long id) {
        tagRepository.deleteById(id);
    }

    public void addTagsToPost(Long postId, List<Long> numbersOfTagId) {
        tagRepository.addTagsToPost(postId, numbersOfTagId);
    }

    public void deleteTagsFromPostByPostId(Long postId, List<Long> numbersOfTagId) {
        tagRepository.deleteTagsFromPostByPostId(postId, numbersOfTagId);
    }

    public void deleteTagsByListOfTagsId(List<Long> numbersOfTagId) {
        tagRepository.deleteTagsByListOfTagsId(numbersOfTagId);
    }
}
