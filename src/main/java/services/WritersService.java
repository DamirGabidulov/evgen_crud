package services;

import models.Writer;
import repositories.PostRepository;
import repositories.jdbc.PostRepositoryImpl;
import repositories.WritersRepository;
import repositories.jdbc.WritersRepositoryImpl;

import java.util.List;
import java.util.Optional;

import static utils.JdbcUtil.getConnection;

public class WritersService {

    private WritersRepository writersRepository;

    public WritersService() {
        this.writersRepository = new WritersRepositoryImpl();
    }

    public void setWritersRepository(WritersRepository writersRepository) {
        this.writersRepository = writersRepository;
    }

    public List<Writer> findAll(){
        return writersRepository.findAll();
    }

    public Optional<Writer> getById(Long id) {
        return writersRepository.getById(id);
    }

    public Writer save(Writer writer) {
        return writersRepository.save(writer);
    }

    public Writer update(Writer writer) {
        return writersRepository.update(writer);
    }

    public void deleteById(Long id) {
        writersRepository.deleteById(id);
    }
}
