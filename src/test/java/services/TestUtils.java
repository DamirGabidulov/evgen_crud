package services;

import models.Post;
import models.Tag;
import models.Writer;

import java.util.Arrays;
import java.util.List;

public class TestUtils {

    public static List<Tag> getTags() {
        return List.of(
            Tag.builder()
                    .id(1L)
                    .name("extremely")
                    .build(),
            Tag.builder()
                    .id(2L)
                    .name("complicated")
                    .build(),
            Tag.builder()
                    .id(3L)
                    .name("dangerous")
                    .build());
    }

    public static List<Post> getPosts () {
        return
                List.of(
                        Post.builder().id(1L).content("news").tags(getTags()).build(),
                        Post.builder().id(2L).content("sport").tags(getTags()).build()
                );
    }

    public static List<Writer> getWriters() {
        return Arrays.asList(
                Writer.builder().id(1L).name("Cormac McCarthy").posts(getPosts()).build(),
                Writer.builder().id(2L).name("Joseph Heller").posts(getPosts()).build()
        );
    }
}