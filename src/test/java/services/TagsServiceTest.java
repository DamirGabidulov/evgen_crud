package services;

import models.Post;
import models.PostStatus;
import models.Tag;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;
import org.mockito.*;
import org.mockito.junit.jupiter.MockitoExtension;
import org.mockito.stubbing.OngoingStubbing;
import repositories.TagRepository;
import repositories.jdbc.TagRepositoryImpl;

import java.util.*;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.when;
import static services.TestUtils.getTags;

@ExtendWith(MockitoExtension.class)
class TagsServiceTest {

    @Mock
    TagRepository tagRepository;

    @InjectMocks
    TagsService tagsService;

    @BeforeEach
    public void setUp() {
        Mockito.lenient().when(tagRepository.findAll()).thenReturn(getTags());
        Mockito.lenient().when(tagRepository.getById(1L)).thenReturn(Optional.of(Tag.builder().id(1L).name("extremely").build()));
        Mockito.lenient().when(tagRepository.save(Tag.builder().name("new tag").build())).thenReturn(Tag.builder().id(4L).name("new name").build());
        Mockito.lenient().when(tagRepository.update(Tag.builder().id(5L).name("old name").build())).thenReturn(Tag.builder().id(5L).name("current name").build());

        tagsService.setTagRepository(tagRepository);
    }

    @Test
    void findAll() {
        assertEquals(getTags(), tagsService.findAll());
    }

    @ParameterizedTest
    @ValueSource(longs = 1L)
    void getById(Long id){
        assertEquals(Optional.of(Tag.builder()
                .id(id)
                .name("extremely")
                .build()), tagsService.getById(1L));
    }

    @Test
    void save(){
        assertEquals(Tag.builder().id(4L).name("new name").build(), tagsService.save(Tag.builder().name("new tag").build()));
    }

    @Test
    void update(){
        assertEquals(Tag.builder().id(5L).name("current name").build(), tagsService.update(Tag.builder().id(5L).name("old name").build()));
    }

    @Test
    void delete(){
        Tag tag = Tag.builder().id(6L).name("current name").build();
        tagsService.delete(tag.getId());
        Mockito.verify(tagRepository).deleteById(tag.getId());
    }

    @Test
    void addTagsToPost(){
        Post post = Post.builder().id(1L).content("something").status(PostStatus.ACTIVE).build();
        Tag firstTag = Tag.builder().id(1L).name("first").build();
        Tag secondTag = Tag.builder().id(2L).name("second").build();
        tagsService.addTagsToPost(post.getId(), Arrays.asList(firstTag.getId(), secondTag.getId()));
        Mockito.verify(tagRepository).addTagsToPost(post.getId(), Arrays.asList(firstTag.getId(), secondTag.getId()));
    }

    @Test
    void deleteTagsFromPostByPostId(){
        Tag firstTag = Tag.builder().id(1L).name("first").build();
        Tag secondTag = Tag.builder().id(2L).name("second").build();
        Post post = Post.builder().id(1L).content("something").tags(Arrays.asList(firstTag, secondTag)).status(PostStatus.ACTIVE).build();
        tagsService.deleteTagsFromPostByPostId(post.getId(), Arrays.asList(firstTag.getId(), secondTag.getId()));
        Mockito.verify(tagRepository).deleteTagsFromPostByPostId(post.getId(), Arrays.asList(firstTag.getId(), secondTag.getId()));
    }

    @Test
    void deleteTagsByListOfTagsId(){
        Tag firstTag = Tag.builder().id(1L).name("first").build();
        Tag secondTag = Tag.builder().id(2L).name("second").build();
        tagsService.deleteTagsByListOfTagsId(Arrays.asList(firstTag.getId(), secondTag.getId()));
        Mockito.verify(tagRepository).deleteTagsByListOfTagsId(Arrays.asList(firstTag.getId(), secondTag.getId()));
    }
}