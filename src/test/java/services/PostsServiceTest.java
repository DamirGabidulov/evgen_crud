package services;

import models.Post;
import models.Writer;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import repositories.PostRepository;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.verify;
import static services.TestUtils.getPosts;
import static services.TestUtils.getTags;

@ExtendWith(MockitoExtension.class)
class PostsServiceTest {

    @Mock
    PostRepository postRepository;


    @InjectMocks
    PostsService postsService;


    @BeforeEach
    void setUp(){
        Mockito.lenient().when(postRepository.findAll()).thenReturn(getPosts());
        Mockito.lenient().when(postRepository.getById(2L)).thenReturn(Optional.of(getPosts().get(1)));
        Mockito.lenient().when(postRepository.save(getPosts().get(0))).thenReturn(Post.builder().id(1L).content("news").tags(getTags()).build());
        Mockito.lenient().when(postRepository.update(getPosts().get(0))).thenReturn(Post.builder().id(1L).content("news").tags(getTags()).build());
        Mockito.lenient().when(postRepository.addNewPostToWriter(1L, Post.builder().id(3L).content("news").tags(getTags()).build())).thenReturn(Post.builder().id(3L).content("news").tags(getTags()).build());
        postsService.setPostRepository(postRepository);
    }


    @Test
    void findAll() {
        assertEquals(getPosts(), postsService.findAll());
    }

    @ParameterizedTest
    @ValueSource(longs = 2L)
    void getById(Long id) {
        assertEquals(Optional.of(getPosts().get(1)), postsService.getById(id));
    }

    @Test
    void save() {
        assertEquals(Post.builder().id(1L).content("news").tags(getTags()).build(), postsService.save(getPosts().get(0)));
    }

    @Test
    void update() {
        assertEquals(Post.builder().id(1L).content("news").tags(getTags()).build(), postsService.update(getPosts().get(0)));
    }

    @Test
    void delete() {
        postsService.delete(getPosts().get(1).getId());
        verify(postRepository).deleteById(getPosts().get(1).getId());
    }

    @Test
    void addNewPostToWriter() {
        Post newPost = Post.builder().id(3L).content("news").tags(getTags()).build();
        assertEquals(newPost, postsService.addNewPostToWriter(1L, newPost));
    }

    @Test
    void deletePostsFromWriterByIds() {
        Writer writer = Writer.builder().id(1L).name("David").posts(Arrays.asList(getPosts().get(0), getPosts().get(1))).build();
        List<Long> numbersOfPostId = writer.getPosts().stream().map(Post::getId).collect(Collectors.toList());
        postsService.deletePostsFromWriterByIds(numbersOfPostId);
        verify(postRepository).deletePostsFromWriterByIds(numbersOfPostId);
    }
}