package services;

import models.Post;
import models.Writer;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import repositories.WritersRepository;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.verify;
import static services.TestUtils.getPosts;
import static services.TestUtils.getWriters;

@ExtendWith(MockitoExtension.class)
class WritersServiceTest {

    @Mock
    WritersRepository writersRepository;

    @InjectMocks
    WritersService writersService;

    @BeforeEach
    void setUp() {
        Mockito.lenient().when(writersRepository.findAll()).thenReturn(getWriters());
        Mockito.lenient().when(writersRepository.getById(1L)).thenReturn(Optional.of(getWriters().get(0)));
        Mockito.lenient().when(writersRepository.save(Writer.builder().name("Cormac McCarthy").posts(getPosts()).build())).thenReturn(Writer.builder().id(1L).name("Cormac McCarthy").posts(getPosts()).build());
        Mockito.lenient().when(writersRepository.update(getWriters().get(1))).thenReturn(Writer.builder().id(2L).name("Joseph Heller Junior").posts(getPosts()).build());
        writersService.setWritersRepository(writersRepository);
    }

    @Test
    void findAll() {
        assertEquals(getWriters(), writersService.findAll());
    }

    @Test
    void getById() {
        assertEquals(Optional.of(getWriters().get(0)), writersService.getById(1L));
    }

    @Test
    void save() {
        Writer cormacMcCarthy = Writer.builder().name("Cormac McCarthy").posts(getPosts()).build();
        assertEquals(Writer.builder().id(1L).name("Cormac McCarthy").posts(getPosts()).build(), writersService.save(cormacMcCarthy));
    }

    @Test
    void update() {
        Writer writer = getWriters().get(1);
        writer.setName("Joseph Heller Junior");
        assertEquals(writer, writersService.update(getWriters().get(1)));
    }

    @Test
    void deleteById() {
        writersService.deleteById(getWriters().get(1).getId());
        verify(writersRepository).deleteById(getWriters().get(1).getId());
    }
}